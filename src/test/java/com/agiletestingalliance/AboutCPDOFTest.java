package com.agiletestingalliance;

import static org.junit.Assert.*;
import org.junit.Test;

public class AboutCPDOFTest {
    @Test
    public void testDesc() throws Exception {
    	AboutCPDOF obj = new AboutCPDOF();
        String k = obj.desc();
        assertTrue("String is wrong", k.contains("CP-DOF certification program covers"));
    }
}