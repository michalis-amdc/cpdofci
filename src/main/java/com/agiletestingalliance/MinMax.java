package com.agiletestingalliance;

public class MinMax {

	public int verifyBiggerNum(int num1, int num2) {
		if (num2 > num1){
			return num2;
		}
		else{
			return num1;
		}
	}

	
	public String bar(String string) {
		if (!string.isEmpty() || !"".equals(string)){
			return string;
		}
		if (string.isEmpty() && "".equals(string)){
			return string;
		}
		return string;
	}
}
